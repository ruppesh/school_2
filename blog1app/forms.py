from django import forms
from .models import *
from django.contrib.auth.models import User



class DateTimeInput(forms.DateInput):
    input_type = 'time'


class UserForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'required': 'true',
                    'placeholder': 'Password...',
    }))

    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'required': 'true',
                    'placeholder': 'Confirm password...',
    }))

    class Meta:
        model = User
        fields = [
            "username",
            "email",
            "password1",
            "password2",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields["username"].widget.attrs.update(
            {'placeholder': 'Username', 'required': 'true'})
        self.fields["email"].widget.attrs.update(
            {'placeholder': 'E-Mail', 'required': 'true'})

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if not password2:
            raise forms.ValidationError("You must confirm your password")
        if password1 != password2:
            raise forms.ValidationError("Your passwords do not match")
        return password2







class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}))



class BlogForm(forms.ModelForm):
	class Meta:
		model = Blog
		fields = '__all__'



class VideoForm(forms.ModelForm):

    class Meta:
        model = Video
        fields = ['url', 'title']
        # widgets = {
        #     'title': forms.TextInput(attrs={
        #         'class': 'form-control',
        #         'placeholder': 'Title',
        #         'required': 'True'}),
        #     'url': forms.TextInput(attrs={
        #         'class': 'form-control',
        #     }),
        # }