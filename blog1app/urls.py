from django.conf.urls import url 
from .views import *




urlpatterns = [


    url(r'^logout/$', Logout.as_view(),
        name='logout'),
    url(r'^login/$', Login.as_view(),
        name='login'),



	url(r'^$', HomeView.as_view(), name='home'),
	url(r'^register/$', RegView.as_view(), name='reg'),
	url(r'^thanku/$', ThankuView.as_view(), name='thanku'),
	url(r'^blogcreate/$', BlogCreateView.as_view(), name='blogcreate'),
	url(r'^videocreate/$', VideoCreateView.as_view(), name='videocreate'),


 ]