from django.views.generic import *
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import *
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse_lazy
from .forms import *
from urllib.parse import urlparse, parse_qs





class Logout(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class Login(FormView):
    template_name = "adminlogin.html"
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_superuser:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'errors': 'Incorrect username or password'})
        return super().form_valid(form)


class HomeView(ListView):
	template_name='clienthome.html'
	model = Blog
	
	def get_context_data(self, *args, **kwargs):
		context = super().get_context_data(*args, *kwargs)
		context['blogs'] = Blog.objects.filter(author=self.request.user)
		context['videos'] = Video.objects.all()
		return context




class RegView(CreateView):
    template_name = 'regform.html'
    form_class = UserForm
    success_url = '/'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password2']
        user = form.save()
        user.set_password(password)
        user.save()

        user = authenticate(username=username, password=password)
        login(self.request, user)
        return super().form_valid(form)


class ThankuView(TemplateView):
	template_name = 'thanku.html'

class BlogCreateView(CreateView):
	template_name = 'blogcreate.html'
	form_class = BlogForm
	success_url = '/thanku/'



class VideoCreateView(CreateView):
    template_name = 'videocreate.html'
    model = Video
    form_class = VideoForm
    success_url = '/'

    def form_valid(self, form):
        url = form.cleaned_data['url']
        url_data = urlparse(url)
        query = parse_qs(url_data.query)
        video = query['v'][0]
        form.instance.url = video
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['url_name'] = 'videos'
        return context

 