from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User



# class TimeStamp(models.Model):
#     created_at = models.DateTimeField(auto_now_add=True, null=True)
#     updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
#     deleted_at = models.DateTimeField(null=True, blank=True)

#     class Meta:
#         abstract = True

#     def delete(self):
#         self.deleted_at = timezone.now()
#         super().save()

class Blog(models.Model):
    title = models.CharField(max_length=500)
    image = models.ImageField(upload_to='blog/')
    content = models.TextField()
    author = models.ForeignKey(User)

    def __str__(self):
        return self.title



class Video(models.Model):
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=255)


    def __str__(self):
        return self.title